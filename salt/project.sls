include:
  - nodejs-package

proj-user:
  user.present:
    - name: project
    - home: /home/project

git-client-package:
  pkg.installed:
    - name: git

code-source:
  git.latest:
    - name: githubrepo.git
    - rev: master
    - target: /home/project/project
    - require:
      - user: proj-user
      - pkg: git-client-package
      - sls: nodejs-package

npm-install:
  cmd.wait:
    - name: npm install
    - cwd: /home/project/project
    - watch:
      - git: code-source

build-script:
  cmd.wait:
    - name: npm run-script build
    - cwd: /home/project/project
    - watch:
      - git: code-source

