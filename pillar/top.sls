base:
  '*':
    - common
    - beacons

  'loadbalance':
    - override
  'roles:load-balancing':
    - match: grain
    - project-ssl
    
   'roles:project-web':
    - match: grain
    - web-mine
