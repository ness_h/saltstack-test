include:
  - project

supervisor:
  pkg.installed:
    - require:
      - sls: project
  service.running:
    - watch:
      - file: /etc/supervisor/conf.d/project.conf

restart-site:
  supervisord.running:
    - name: project
    - watch:
      - sls: project

/etc/supervisor/conf.d/project.conf:
  file.managed:
    - source: salt://project/supervisor.conf
    - require:
      - pkg: supervisor
