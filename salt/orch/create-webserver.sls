set-role:
  salt.function:
    - name: grains.append
    - tgt: {{ pillar['target-minion'] }}
    - arg:
      - roles
      - project-web

apply-states:
  salt.state:
    - tgt: {{ pillar['target-minion'] }}
    - sls: project
    - require:
      - salt: set-role

refresh-pillar:
  salt.function:
    - name: saltutil.refresh_pillar
    - tgt: {{ pillar['target-minion'] }}
    - require:
      - salt: apply-states

update-mine:
  salt.function:
    - name: mine.update
    - tgt: {{ pillar['target-minion'] }}
    - require:
      - salt: refresh-pillar

update-project-online-grain:
  salt.function:
    - name: grains.append
    - arg:
      - project
      - online
    - require:
      - salt: update-mine

run-state-load-balancer:
  salt.state:
    - tgt: roles:load-balancing
    - tgt_type: grain
    - sls: load-balance
    - require:
      - salt: update-project-online-grain
