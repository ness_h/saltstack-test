nginx:
  pkg.installed: []
  service.running:
    - watch:
      - file: /etc/nginx/nginx.conf
      - file: {{ pillar['project-ssl']['cert-path'] }}
      - file: {{ pillar['project-ssl']['cert-key-path'] }}


/etc/nginx/nginx.conf:
  file.managed:
    - source: salt://load-balance/nginx.conf
    - template: jinja

{{ pillar['project-ssl']['cert-path'] }}:
  file.managed:
    - contents_pillar: project-ssl:cert-contents

{{ pillar['project-ssl']['cert-key-path'] }}:
  file.managed:
    - contents_pillar: project-ssl:cert-key-contents
