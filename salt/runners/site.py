import salt.client
import requests

client = salt.client.get_local_client('/etc/salt/master')

def is_up():
    webservers = client.cmd('*', 'network.ip_addrs', timeout=1)
    
    allUp = True
    
    for key,value in webservers.iteritems():
        print("IP for {}: {}".format(key,value[0]))
        response = requests.get('http://'+value[0])
        if response.status_code == 200:
            print("Host UP")
        else:
            allUp = False
            print("Host down")
            
    return allUp
        