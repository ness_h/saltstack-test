beacons:
  service:
    nginx:
      onchangeonly: True
  inotify:
    /etc/nginx/nginx.conf: {}
    interval: 5
    disable_during_state_run: True
  diskusage:
    - /: 40%
    - interval: 60